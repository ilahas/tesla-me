
    var myFullpage = new fullpage('#fullpage', {
        navigation: true,
        navigationPosition: 'left',
        navigationTooltips: ['MODEL S', 'SAFETY', 'PERFORMANCE', 'RANGE', 'AUTOPILOT', 'INTERIOR', 'EXTERIOR', 'SPECS', 'ORDER'],
        scrollBar: false,
        afterLoad: function(origin, destination, direction){
            //section 5
            if(destination.index == 0){
            	$('#section5 .videoText .opNone').addClass('cssanimation fadeInBottom');
            	$('#section5 .configuration .opNone').addClass('cssanimation fadeIn');

                $('#section5 .img1').addClass('fadeInBottomS');
                $('#section5 .img2').addClass('fadeInBottomS2');
                $('#section5 .speedometer-circle-inner').addClass('rotateS');
                $('#section5 .speedometer-needle').addClass('rotateN');
            }
            //back to original state
            else if(origin && origin.index == 0){
            	$('#section5 .videoText .opNone').removeClass('cssanimation fadeInBottom');
            	$('#section5 .configuration .opNone').removeClass('cssanimation fadeIn');

                $('#section5 .img1').removeClass('fadeInBottomS');
                $('#section5 .img2').removeClass('fadeInBottomS2');
                $('#section5 .speedometer-circle-inner').removeClass('rotateS');
                $('#section5 .speedometer-needle').removeClass('rotateN');
            }

            //section 6
            if(destination.index == 1){
            	$('#section6 .opNone').addClass('cssanimation fadeInBottom');
            }
            //back to original state
            else if(origin && origin.index == 1){
            	$('#section6 .opNone').removeClass('cssanimation fadeInBottom');
            }

            //section 7
            if(destination.index == 2){
            	$('#section7 .opNone').addClass('cssanimation fadeInBottom');
                $('#section7 .about').addClass('cssanimation fadeInBottom_About');
            }
            //back to original state
            else if(origin && origin.index == 2){
            	$('#section7 .opNone').removeClass('cssanimation fadeInBottom');
                $('#section7 .about').removeClass('cssanimation fadeInBottom_About');
            }

            //section 8
            if(destination.index == 3){
            	$('#section8 .opNone').addClass('cssanimation fadeInBottom');
            }
            //back to original state
            else if(origin && origin.index == 3){
            	$('#section8 .opNone').removeClass('cssanimation fadeInBottom');
            }

            //section 9
            if(destination.index == 4){
            	$('#section9 .opNone').addClass('cssanimation fadeInBottom');
                $('#section9 .about').addClass('cssanimation fadeInBottom_About');
            }
            //back to original state
            else if(origin && origin.index == 4){
            	$('#section9 .opNone').removeClass('cssanimation fadeInBottom');
                $('#section9 .about').removeClass('cssanimation fadeInBottom_About');
            }

             //section 10
            if(destination.index == 5){
            	$('#section10 .opNone').addClass('cssanimation fadeInBottom');
                $('#section10 .about').addClass('cssanimation fadeInBottom_About');
            }
            //back to original state
            else if(origin && origin.index == 5){
            	$('#section10 .opNone').removeClass('cssanimation fadeInBottom');
                $('#section10 .about').removeClass('cssanimation fadeInBottom_About');
            }

             //section 11
            if(destination.index == 6){
            	$('#section11 .opNone').addClass('cssanimation fadeInBottom');
                $('#section11 .about').addClass('cssanimation fadeInBottom_About');
            }
            //back to original state
            else if(origin && origin.index == 6){
            	$('#section11 .opNone').removeClass('cssanimation fadeInBottom');
                $('#section11 .about').removeClass('cssanimation fadeInBottom_About');
            }

            //section 12
            if(destination.index == 7){
            	$('#section12 .opNone').addClass('cssanimation fadeInBottom');
            	$('#section12 .tab-content .opNone').addClass('cssanimation fadeInBottom_10');
            }
            //back to original state
            else if(origin && origin.index == 7){
            	$('#section12 .opNone').removeClass('cssanimation fadeInBottom');
            	$('#section12 .tab-content .opNone').removeClass('cssanimation fadeInBottom_10');
            }


             //section 13
            if(destination.index == 8){
            	$('#section13 .opNone').addClass('cssanimation fadeInBottom');
            	$('#section13 .pageElement .opNone').addClass('cssanimation fadeInBottom_10');
            }
            //back to original state
            else if(origin && origin.index == 8){
            	$('#section13 .opNone').removeClass('cssanimation fadeInBottom');
            	$('#section13 .pageElement .opNone').removeClass('cssanimation fadeInBottom_10');
            }

            
        }
    });

     document.querySelector('#goDown').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveSectionDown();
	});

	document.querySelector('#goUp6').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp7').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp8').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp9').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp10').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp11').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp12').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	document.querySelector('#goUp13').addEventListener('click', function(e){
		e.preventDefault();
		fullpage_api.moveTo(1);
	});

	$(document).ready(function() {
	    $(document).on("click","#openSide",function() {
		    setTimeout(
			  function() 
			  {
			    $('#sidebar').addClass('opened');
		    	$('#closeSide').addClass('open');

			  }, 130
			);
	    });

	    $(document).on("click","#closeSide",function() {
	    	$('#closeSide').removeClass('open');
			
			setTimeout(
			  function() 
			  {
			  	$('#sidebar').removeClass('opened');
			    $('#nav-icon3').removeClass('open');
			  }, 130
			);
	    });
	});

	$(document).ready(function(){
		$('#nav-icon3').click(function(){
			$(this).toggleClass('open');
		});

		$('.btnTab').trigger('click');
	});


    
